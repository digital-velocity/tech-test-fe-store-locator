#Tech Test - FE - Store Locator#

##Client Request:##
The client would like to add a store locator to their website, so that their customers can quickly find their local store.

The client currently only sells to customers within the UK, but only has stores within a 20-30 mile radius (West Yorkshire region).

They would like it to be placed below the products on the index.html, and above the footer.

They would like the store locator to respond, and be intuitive on Mobile devices.

##Success criteria:##
- The customer can either enter their postcode, or click 'current location' and get back a useful list of stores (or be shown their closest store, within reason...).
- The store locator style must match the designs, or improve upon the designs set out by the design agency.
- The store locator must be intuitive to use, and have similar design principles to the rest of the site.

---------------------------------

###File structure:###

-> **index.html** - Please do your HTML work here

-> **source** - Directory containing the source files for the current site (js, css, etc...). **Do not edit these files.**

-> **working** - Directory, please put **your** working files (js, css, less, etc...) here.

-> **psds** - Directory containing the source design files.

---------------------------------

###Design files:###
The client has had the store locator designed by an external design agency. They have provided a view of the store locator, that doesn't include all elements that will be required to make the store locator fully functional.

The PSD's for the project are contained within the project directory.

---------------------------------

###Branch Search (on Latitude & Longitude):###

####Description:####
This call will be used to return an array of branch objects, depending on the input of latitude and longitude data.

####URL:####
https://tech-test.secure-update.co.uk/branch/json-search

####Variables (GET):####

Key | Description
------------ | -------------
**lat** | Customers current search latitude
**lng** | Customers current search longitude
**distance** | Radius in which to return search results for
**limit** | Count of stores to return in call
**collect** | Does the store have collect from store available

####Example response:####
```json
[  
	{  
		"branch_id":"6",
		"branch":"Wakefield Trinity Store",
		"slug":"wakefield_trinity_store_wakefield",
		"distance":1.66,
		"address":{  
			"address_1":"Trinity shopping centre",
			"address_2":"",
			"address_3":"",
			"address_4":"wakefield",
			"address_5":"",
			"postcode":"wf1 1qr"
		},
		"geo":{  
			"latitude":"53.68492",
			"longitude":"-1.49661"
		},
		"profile":{  
			"title":"",
			"description":"",
			"unit_type":"store"
		},
		"contact":{  
			"email":"email@shop.com",
			"phone":""
		},
		"opening_times":{  
			"monday":"09:00AM - 05:30PM",
			"tuesday":"09:00AM - 05:30PM",
			"wednesday":"09:00AM - 05:30PM",
			"thursday":"09:00AM - 05:30PM",
			"friday":"09:00AM - 05:30PM",
			"saturday":"09:00AM - 05:30PM",
			"sunday":"10:30AM - 04:00PM"
		},
		"links":{  
			"google_map_query":"https:\/\/www.google.co.uk\/maps?q=53.68492,-1.49661"
		}
	}
]
```

---------------------------------

##Contacts:##
**David Ford** (Digital Director) - [d.ford@digital-velocity.co.uk](d.ford@digital-velocity.co.uk)